requires "Mojolicious", ">=6.12";
requires "IO::Socket::SSL", ">= 1.84";
requires "Time::HiRes";
test_requires "Test::Most";
test_requires "Test::Pretty";
