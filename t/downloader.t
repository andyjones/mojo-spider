use Test::Most;

local $ENV{MOJO_LOG_LEVEL} = 'error'; # suppress Mojo::Log

use_ok('Mojo::Spider::Downloader');

BEGIN {
    package ExampleApp;

    use Mojolicious::Lite;

    get '/page1' => sub {
        shift->render( text => '<div>Page 1</div>' );
    };

    get '/page2' => sub {
        shift->render( text => '<div>Page 2</div>' );
    };
};

subtest "it emits DOMs after successful downloads" => sub {
    my $downloader = Mojo::Spider::Downloader->new();
    my ($got_url, $got_dom);
    $downloader->on(download => sub {
        (undef, $got_url, $got_dom) = @_;
    });
    $downloader->start('/page1');
    $downloader->wait;

    is $got_url            => '/page1';
    is $got_dom->at('div')->text => 'Page 1';
};

subtest "it doesn't emit the DOM after failed downloads" => sub {
    my $downloader = Mojo::Spider::Downloader->new();
    my $called = 0;
    $downloader->on(download => sub { $called++ });

    $downloader->start('/404')->wait;

    ok !$called;
};

subtest "it downloads in parallel" => sub {
    my $downloader = Mojo::Spider::Downloader->new();
    my %got;
    $downloader->on(download => sub {
        my ($self, $url, $dom) = @_;
        $got{$url}++;
    });
    $downloader->start('/page1');
    $downloader->start('/page2');
    $downloader->wait;

    is $got{'/page1'} => 1;
    is $got{'/page2'} => 1;
};

subtest "it emits 'after_error' when downloads fail" => sub {
    my $downloader = Mojo::Spider::Downloader->new();
    my $error_received;
    $downloader->on(after_error => sub {
        my ($self, $url, $tx, $error) = @_;
        $error_received = $error;
    });
    $downloader->start('/404');
    $downloader->wait;

    is $error_received->{code} => 404;
};

done_testing;
