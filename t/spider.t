use Test::Most;
use Mojo::DOM;

BEGIN {
    package Mock::Downloader;

    use Mojo::Base 'Mojo::EventEmitter';

    sub start {
        my ($self, $url) = @_;

        push @{$self->{urls_received} ||= [] }, $url;
    }
    sub wait { return; }

    sub urls_received { @{ shift->{urls_received} ||= [] } }
};

BEGIN {
    package Mock::Explorer;

    use Mojo::Base 'Mojo::EventEmitter';

    sub accept {
        my ($self, $site_url, @next_urls) = @_;

        push @{$self->{urls_received} ||= [] }, @next_urls;
    }

    sub urls_received { @{ shift->{urls_received} ||= [] } }
};

use_ok('Mojo::Spider');

subtest "it provides the downloader with the starting url" => sub {
    my $downloader = Mock::Downloader->new();
    my $spider = Mojo::Spider->new({
        downloader => $downloader,
    });

    $spider->start('http://www.example.com/');

    is_deeply [$downloader->urls_received] => ['http://www.example.com/'];
};

subtest 'it notifies the downloader when there are pages to download' => sub {
    my $downloader = Mock::Downloader->new;
    my $spider = Mojo::Spider->new({
        downloader => $downloader,
    });

    $spider->explorer->emit('new_url' => 'https://www.example.com');

    is_deeply [$downloader->urls_received] => ['https://www.example.com'];
};

subtest 'it notifies the explorer after it has downloaded a page' => sub {
    my $downloader = Mock::Downloader->new();
    my $explorer   = Mock::Explorer->new();
    my $spider = Mojo::Spider->new({
        downloader => $downloader,
        explorer   => $explorer,
    });

    my $sample_dom = Mojo::DOM->new('<a href="/index.html">Index</a>');
    $downloader->emit('download' => 'https://www.example.com', $sample_dom);

    is_deeply [$explorer->urls_received],
              ['/index.html'];
};

subtest 'it emits a scraped event after every page is scraped' => sub {
    my $downloader = Mock::Downloader->new();
    my $spider = Mojo::Spider->new({
        downloader => $downloader,
    });

    my $url_scraped;
    $spider->on('scraped', sub {
        my ($self, $url, $scrape) = @_;
        $url_scraped = $url;
    });

    $downloader->emit('download' => 'https://www.example.com/', Mojo::DOM->new);

    is $url_scraped => 'https://www.example.com/';
};

subtest 'it emits an after_error event after download errors' => sub {
    my $downloader = Mock::Downloader->new();
    my $spider = Mojo::Spider->new({
        downloader => $downloader,
    });

    my $error_received;
    $spider->on('after_error', sub {
        my ($self, $url, $error) = @_;
        $error_received = $error;
    });

    my $tx;
    my $error_sent = { code => 404, message => 'Page not found' };
    $downloader->emit('after_error' => 'https://www.example.com', $tx, $error_sent);

    is_deeply $error_received => $error_sent;
};

done_testing;
