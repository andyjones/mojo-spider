use Test::Most;

my $F = 'Mojo::Spider::Explorer';
use_ok($F);

my $site = 'https://www.example.com';
subtest "it returns urls that are on the same domain" => sub {
    ok $F->new->allowed($site, "$site/path");
};

subtest "it supports protocol-less uris" => sub {
    is $F->new->allowed($site, "//www.example.com/path"),
       'https://www.example.com/path';
};

subtest "it discards urls that are on a different domain" => sub {
    ok !$F->new->allowed($site, "https://www.facebook.com");
};

subtest "it discards urls that are a subdomain" => sub {
    ok !$F->new->allowed($site, "https://subdomain.example.com/url");
};

subtest "it discards urls that are on a different protocol" => sub {
    ok !$F->new->allowed($site, "http://www.example.com/url");
};

subtest "it emits unique urls" => sub {
    my $filter = $F->new;
    my $urls_emitted = 0;
    $filter->on(new_url => sub {
        $urls_emitted++;
    });
    $filter->accept('http://www.example.com', 'http://www.example.com');
    $filter->accept('http://www.example.com', 'http://www.example.com');

    is $urls_emitted => 1;
};

subtest "it attempts to normalise urls" => sub {
    my $filter = $F->new;
    my $urls_emitted = 0;
    $filter->on(new_url => sub {
        $urls_emitted++;
    });
    $filter->accept( 'http://www.example.com',
        qw(http://www.example.com http://www.example.com/
           http://www.EXAMPLE.com)
    );

    is $urls_emitted => 1;
};

done_testing;
