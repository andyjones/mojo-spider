use Test::Most;
use Mojo::DOM;

use_ok('Mojo::Spider::Scraper');

subtest "it can extract absolute urls from a webpage" => sub {
    my $dom = Mojo::DOM->new(
        q{<a href="https://www.example.com/url">anchor text</a>},
    );

    my $scraper = Mojo::Spider::Scraper->new({ dom => $dom });
    is_deeply [$scraper->urls],
              ['https://www.example.com/url'];
};

subtest "it can extract relative urls from a webpage" => sub {
    my $dom = Mojo::DOM->new(
        q{<a href="/url">anchor text</a>},
    );

    my $scraper = Mojo::Spider::Scraper->new({ dom => $dom });
    is_deeply [$scraper->urls], ['/url'];
};

subtest "it can extract the javascript assets from a webpage" => sub {
    my $dom = Mojo::DOM->new(
        q{<script src="//cdn.optimizely.com/js/125150657.js"></script>}
    );

    my $scraper = Mojo::Spider::Scraper->new({ dom => $dom });
    is_deeply [$scraper->static_assets],
              ['//cdn.optimizely.com/js/125150657.js'];
};

subtest "it can extract the css assets from a webpage" => sub {
    my $dom = Mojo::DOM->new(
        q{<link rel="stylesheet" href="/css/main.css">}
    );

    my $scraper = Mojo::Spider::Scraper->new({ dom => $dom });
    is_deeply [$scraper->static_assets],
              ['/css/main.css'];
};

subtest "links to google+ are not static assets!" => sub {
    my $dom = Mojo::DOM->new(
        q{<link href="https://plus.google.com/+Gocardless" rel="publisher">}
    );

    my $scraper = Mojo::Spider::Scraper->new({ dom => $dom });
    is_deeply [$scraper->static_assets] => [];
};

subtest "it can extract images from a webpage" => sub {
    my $dom = Mojo::DOM->new(
        q{<img alt="Footer logos" class="footer__logos" src="/images/footer/footer-logos@2x.png">},
    );

    my $scraper = Mojo::Spider::Scraper->new({ dom => $dom });
    is_deeply [$scraper->static_assets],
              ['/images/footer/footer-logos@2x.png'];
};

done_testing;
