use Test::Most;

use Mojo::UserAgent;
use Mojo::Spider;

local $ENV{MOJO_LOG_LEVEL} = 'error';

BEGIN {
    package ExampleApp;

    use Mojolicious::Lite;
};

subtest "ensure our example app is serving static files" => sub {
    my $ua = Mojo::UserAgent->new;
    is $ua->get('/index.html')->res->dom->at('title')->text, 'Example App Home Page';
};

subtest "it visits every page on the site" => sub {
    my $spider = Mojo::Spider->new;

    my @pages_spidered;
    $spider->on('scraped', sub {
        my ($self, $url, $scrape) = @_;
        push @pages_spidered, $url;
    });

    my $base_url = $spider->downloader->ua->server->nb_url;
    $spider->start($base_url.'index.html');

    is_deeply [ sort @pages_spidered ],
              [ sort map { $base_url.$_ } qw(index.html level1.html level2.html) ];
};

done_testing;
