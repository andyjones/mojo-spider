Mojo::Spider
============

Web spider written in perl using the [Mojolicious framework](https://metacpan.org/release/Mojolicious).

Installation into system perl
-----------------------------

```
# install the dependencies
./vendor/cpanm --installdeps .

# and run
./bin/mojo-spider https://example.com
```

Local installation
------------------

```
# install the dependencies into a local folder
./vendor/cpanm -L local --installdeps .

# and run
perl -Ivendor -Mlocal::lib=local ./bin/mojo-spider https://example.com
```

Tests
-----

```
prove -lr t/
```
