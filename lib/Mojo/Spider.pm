package Mojo::Spider;

use Mojo::Base 'Mojo::EventEmitter';

use Mojo::Spider::Scraper;

has 'downloader' => sub {
    require Mojo::Spider::Downloader;
    return Mojo::Spider::Downloader->new;
};

has 'explorer'   => sub {
    require Mojo::Spider::Explorer;
    return Mojo::Spider::Explorer->new;
};

sub new {
    my $self = shift->SUPER::new(@_);
    return $self->setup_event_listeners;
}

sub setup_event_listeners {
    my $self = shift;

    my $downloader = $self->downloader;
    my $explorer   = $self->explorer;

    # notify the downloader whenever the explorer finds a link
    $explorer->on('new_url' => sub {
        my ($explorer, $new_url) = @_;

        $downloader->start($new_url);
    });

    # notify the explorer whenever the downloader has grabbed a page
    $downloader->on('download' => sub {
        my ($downloader, $url, $dom) = @_;

        my $scrape = Mojo::Spider::Scraper->new({ dom => $dom });
        $self->emit('scraped', $url, $scrape);
        $explorer->accept($url, $scrape->urls);
    });

    # re-emit any download errors
    $downloader->on('after_error' => sub {
        my ($downloader, $url, $tx, $tx_error) = @_;
        $self->emit('after_error', $url, $tx_error);
    });

    return $self;
}

sub start {
    my ($self, $starting_url) = @_;

    # start downloading from the starting url
    $self->explorer->visit($starting_url);

    # loop until there are no more pages to download
    $self->downloader->wait;
}

1;