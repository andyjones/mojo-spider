package Mojo::Spider::Scraper;

# ABSTRACT: Extracts urls and static assets from html

use Mojo::Base '-base';
use Mojo::Collection;

has dom => sub { die "DOM required" };

sub urls {
    my ($self) = @_;
    return $self->_collect_attr('a[href]' => 'href');
}

sub static_assets {
    my ($self) = @_;

    return Mojo::Collection->new(
        $self->_image_assets,
        $self->_js_assets,
        $self->_css_assets,
    )->compact->each;
}

sub _image_assets { shift->_collect_attr('img[src]'    => 'src'); }
sub _js_assets    { shift->_collect_attr('script[src]' => 'src'); }
sub _css_assets   { shift->_collect_attr('link[rel="stylesheet"]' => 'href'); }

sub _collect_attr {
    my ($self, $selector, $attr) = @_;
    return $self->dom->find($selector)->map('attr' => $attr)->each;
}

1;
