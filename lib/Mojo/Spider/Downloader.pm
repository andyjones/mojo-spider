package Mojo::Spider::Downloader;

# ABSTRACT: download urls using Mojo::UserAgent in parallel

use Mojo::Base 'Mojo::EventEmitter';

has ua => sub {
    require Mojo::UserAgent;
    return Mojo::UserAgent->new;
};

has delay => sub {
    require Mojo::IOLoop;
    return Mojo::IOLoop->delay;
};

sub start {
    my ($self, $url) = @_;

    my $end = $self->delay->begin;
    $self->ua->get($url, sub {
        my ($ua, $tx) = @_;

        if ( $tx->success ) {
            $self->emit(download => $url, $tx->res->dom);
        }
        else {
            $self->emit(after_error => $url, $tx, $tx->error);
        }
        $end->();
    });
    return $self;
}

sub wait {
    my $self = shift;
    return $self->delay->wait;
}

1;
