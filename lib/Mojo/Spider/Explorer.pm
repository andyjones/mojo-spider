package Mojo::Spider::Explorer;

# ABSTRACT: Emits urls that we are allowed to crawl
use Mojo::Base 'Mojo::EventEmitter';

use Mojo::URL;

has 'visited' => sub { +{} };

sub allowed {
    my ($self, $site_url, @urls) = @_;

    $site_url = Mojo::URL->new($site_url);
    my @allowed = grep { _same_site($_, $site_url) }
                  map  { _abs_url($_, $site_url)   }
                  @urls;
    return @allowed == 1 ? $allowed[0] : @allowed;
}

sub accept {
    my ($self, $site_url, @urls) = @_;

    foreach my $url ( $self->allowed($site_url, @urls) ) {
        $self->visit($url);
    }
}

sub visit {
    my ($self, $url) = @_;

    $url = _normalise_url($url);

    return if $self->visited->{$url};

    $self->visited->{$url}++;
    $self->emit('new_url' => $url);
}

sub _same_site {
    my ($abs_url, $site_url) = @_;

    return ($abs_url->protocol || '') eq ($site_url->protocol || '')
        && ($abs_url->host || '') eq ($site_url->host || '');
}

sub _abs_url {
    my ($url, $base) = @_;
    return Mojo::URL->new($url)->to_abs($base);
}

sub _normalise_url {
    my $url = shift;

    # rewrite http://www.example.com to http://www.example.com/
    $url = Mojo::URL->new($url);
    if ( $url->path eq '' ) {
        $url->path('/');
    }
    return $url->to_string;
}

1;
